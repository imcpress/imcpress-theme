<?php 
	$user = get_queried_object();
	if ( is_author() ) {
		if ( is_group($user->data->ID) ) {
			$is_group = true;
		} else {
			wp_safe_redirect('/');
		}
	} else {
		$is_group = false;
	}

	if ( is_tag() )
	{
		// Boring WP not processing tags as other taxonomies
		global $wp_query;
		$wp_query->is_tax = true;
		$taxonomy = 'post_tag';
		$term = get_query_var( 'tag' );
	}
	
	$has_asides = is_tax() && !get_query_var('wire') && !is_post_type_archive() && apply_filters( 'only_post_on_tax_archive', true);
?>
<?php get_header(); ?>

<div class="row <?= $has_asides ? 'double':'single' ?>">
	<div id="page-container">
		<main>
            <section>
				<header>
					<h1><?= get_the_archive_title() ?></h1>
			<?php
			// Fix: check if avatar is not default?
			if ( $is_group )
			{
				// Fix: get actual default avatar?
				if ('user.png' != basename(get_avatar_url($post->post_author)) )
				{
					echo get_avatar($post->post_author);
				}
			}
			elseif ( is_post_type_archive('post'))
			{
				// Display wire terms if on /articles/
				$wires = get_terms('wire');
				foreach ($wires as $wire)
				{
					echo '<a href="'.get_term_link($wire).'" class="tag">'.$wire->name.'</a>';
				}
				unset($wires, $wire);
			}
			elseif ( is_post_type_archive('imcpress_event'))
			{
				get_template_part( 'template-parts/events/events_nav' );
			}
			
			$description = get_the_archive_description();
			if ( !empty( $description ) )
			{
				echo "<p>$description</p>";
			}

			if ( $is_group ) {
				next_group_events( $author );
			}
			
			echo '<nav class="pagination-container">'.paginate_links().'</nav>';
			echo '</header>';
			echo '<hr>';
	
if ( have_posts() ) :
	while ( have_posts() ) : the_post(); ?>
		
		<article class="post">
			<h2><?php 
			if ( !is_post_type_archive() && !get_query_var('wire') &&
					!( is_tax() && apply_filters( 'only_post_on_tax_archive', true ) )
				)
				{
					$type = get_post_type();
					$type = get_post_type_object( $type );
					$type = $type->name;
					if ( $type !== 'imcpress_tumble' ) {
						printf( '<img class="type-icon" src="%s.png" alt="%s"/>', ICONS_URI.strtolower($type), $type );
					} else {
						$icon = sprintf( '<img class="type-icon" src="%s.png" alt="%s"/>', ICONS_URI.strtolower($type), $type );
					}
				}
				?><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
			<p class="post-meta">
				<?php
				
				if ( isset( $icon ) ) {
					echo $icon;
					unset( $icon );
				}
				posted_by_on();

				// Don't display term we're listing from
				$filter = (string) single_term_title('', false);
				
				list_tax(get_the_id(), 'wire', null, $filter);
				list_tax(get_the_id(), 'post_tag', null, $filter);
				list_tax(get_the_id(), 'place_tag', null, $filter);
				
				?>
			</p>
			<div class="flex wrap">
				<?php
					the_post_thumbnail('medium', array( 'class' => 'thumbnail no-margin') );
				?>
				<div class="wrap-300">
				<?php
					if ( is_event() )
					{
						event_card();
					}
				?>
				<?php the_excerpt() ?>
				</div>
			</div>
			<a href="<?= get_the_permalink() ?>" class="plus"><?= __( 'Read more', 'imcpress-theme') ?> →</a>
		</article>
	
	<?php endwhile;
		echo '<nav class="pagination-container">'.paginate_links().'</nav>';

else:
	printf( '<p>%s</p>', __( 'There are no posts!', 'imcpress-theme' ) );
endif;

?>
            </section>
        </main>
    </div>
	<?php
	if ( $has_asides )
	{
		echo '<div>';
		printf( '<aside class="block aux-block"><h2>Other results for "%s"</h2></aside>', get_the_archive_title() ); // Fix: change wording?

		// Fix: factorize by getting all posts types? (and check their taxonomies)
		// Fix: use another template? Display only title? show taxonomy somewhere? taxonomies?
		// show event date+place?
		$tax = \IMCPress\Rewrite::get_tax_rewrite_slug( $taxonomy );
		get_template_part('template-parts/home/events', null, [
			'query' => [
				'tax_query'	=> array(
					array(
						'taxonomy'	=> $taxonomy,
						'field'		=> 'slug',
						'terms'		=> $term,
					),
				),
				'meta_key'	=> 'imcpress_event_metadata_date',
				'meta_value' => '',
				'orderby' => 'meta_value',
				'order'	=> 'ASC',
				'posts_per_page'	=> 4,
				'no_found_rows'		=> true,
			],
		]);

		get_template_part('template-parts/home/simple-block', null, [
			'query' => [
				'post_type'		=> 'imcpress_tumble',
				'post_status'	=> [
					'publish', 'feature'
				],
				'tax_query'	=> array(
					array(
						'taxonomy'	=> $taxonomy,
						'field'		=> 'slug',
						'terms'		=> $term,
					),
				),
				'orderby' => 'post_date',
				'order'	=> 'DESC',
				'posts_per_page'	=> '4',
				'no_found_rows'		=> true,
			],
			'slug'  => 'tumbles',
			'title'  => 'Tumbles',
			'link'  => trailingslashit( get_post_type_archive_link('imcpress_tumble') )."$tax/$term",
			'icon'  => 'news2.png',
			'display_title'	=> false,
			'class'	=> array( 'majenta' ),

		]);
		
		// If about (no place_tag on zines)
		if ( 'post_tag' == $taxonomy )
		{
			get_template_part('template-parts/home/simple-block', null, [
				'query' => [
					'post_type'		=> 'imcpress_zine',
					'post_status'	=> [
						'publish', 'feature'
					],
					'tax_query'	=> array(
						array(
							'taxonomy'	=> $taxonomy,
							'field'		=> 'slug',
							'terms'		=> $term,
						),
					),
					'orderby' => 'post_date',
					'order'	=> 'DESC',
					'posts_per_page'	=> '4',
					'no_found_rows'		=> true,
				],
				'slug'  => 'zines',
				'title'  => 'Zines',
				'link'  => trailingslashit( get_post_type_archive_link('imcpress_zine') )."$tax/$term",
				'icon'  => 'zine.png',
				'display_title'	=> true,
				// 'class'	=> array( 'majenta' ),

			]);

		}
	}

	echo '</div>';
	?>
</div>

<?php get_footer();