<?php

if ( post_password_required() )
    return;

if ( ! post_type_supports( get_query_post_type(), 'comments') && ! have_comments() )
	return;

?>
 
<div id="comments" class="comments-area">
<h2 class="comments-title center"><?= __( 'Comments') ?></h2>
<?php 
    $status = \IMCPress\Comment\Comment::get_custom_post_comment_status( get_post( get_the_ID() ) );
    if ( $status )
    {
        _e( 'Comments are moderated a posterio.', 'imcpress-theme' );
    }
    else
    {
        _e( 'Comments are moderated a priori.', 'imcpress-theme' );
    }
?>
 
    <?php if ( have_comments() ) : ?>
 
        <ol class="comment-list">
            <?php
                wp_list_comments( array(
                    'style'         => 'ol',
                    'walker'        => new \IMCPress\Comment\Walker,
                ));
            ?>
        </ol><!-- .comment-list -->
 
        <?php
            // Are there comments to navigate through?
            if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
        ?>
        <nav class="navigation comment-navigation" role="navigation">
            <h1 class="screen-reader-text section-heading"><?php _e( 'Comment navigation', 'imcpress-theme' ); ?></h1>
            <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'imcpress-theme' ) ); ?></div>
            <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'imcpress-theme' ) ); ?></div>
        </nav><!-- .comment-navigation -->
        <?php endif; // Check for comment navigation ?>
 
        <?php if ( ! comments_open() && get_comments_number() ) : ?>
        <p class="no-comments"><?php _e( 'Comments are closed.' , 'imcpress-theme' ); ?></p>
        <?php endif; ?>
 
    <?php endif; // have_comments() ?>

	<?php
	if ( comments_open( get_the_ID() ) ) { 
		if ( isset( $_GET['replytocom'] ) ) { ?>
		<details class="comment-details" id="comment-form-details" open>
			<summary><?php _e( 'Leave a Comment' ); ?></summary>
			<div id="respond"><?php comment_form(); ?></div>
		</details>
		<?php } else { ?>
		<details class="comment-details" id="comment-form-details" data-action="imcpress_comment_form" data-ajaxurl="<?php echo admin_url( 'admin-ajax.php' ); ?>" data-nonce="<?php echo wp_create_nonce('imcpress_comment_form'); ?>" data-postid="<?php echo get_the_ID(); ?>">
			<summary><?php _e( 'Leave a Comment' ); ?></summary>
			<div id="comment-form-details-content"></div>
		</details>
	<?php 		
		}
	} ?>
 
</div><!-- #comments -->