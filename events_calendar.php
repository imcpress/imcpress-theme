<?php get_header(); ?>

<div class="row single">
	<div id="page-container">
		<main>
            <section>
            	<header>
                    <h1><?= __( 'Monthly calendar', 'imcpress-theme' ) ?></h1>
                    <?php // Also set $year and $month
                    get_template_part( 'template-parts/events/events_nav' );
                    get_template_part('template-parts/events/month_nav') ?>
                </header>
                
                <div class="post-content">
                <?php 
                    
                    // Groups events by date
                    $events = [];
                    while (have_posts())
                    {
                        the_post();

                        $id = get_the_ID();
                        $date = get_post_meta( $id, 'imcpress_event_metadata_date', true);
                        $time = (new DateTime($date))->format('H:i');
                        $day = substr($date, 8, 2);

                        $events[$day][$id] = array( 
                            'date'  => $date,
                            'time'  => $time,
                            'title' => get_the_title(),
                            'place' => get_post_meta( $id, 'imcpress_event_metadata_place', true),
                            'link'  => get_the_permalink(),
                        );
                    }
                ?>
                
                    <div id="calendar">
                        <div class="flex calendar-header">
                        <?php // l10n: replace by localed days ?>
                            <div>Lundi</div>
                            <div>Mardi</div>
                            <div>Mercredi</div>
                            <div>Jeudi</div>
                            <div>Vendredi</div>
                            <div>Samedi</div>
                            <div>Dimanche</div>
                        </div>
                    <?php
                    
                    // Fix: l10n: use WP setting for day starting week

                    $month_obj = new DateTime($year.'-'.$month.'-01');
                    $day_int = new DateInterval('P1D');
                    $now = (new DateTime)->format('d/m/Y');
                    
                    while (true)
                    {
                        echo '<div class="calendar-week flex">';
                        
                        for ($i = 1 ; $i <= 7 ; $i++)
                        {
                            // Check day of the week is the same as current column
                            // and if we're still in the month
                            $dow = (int) $month_obj->format('N');
                            if ($dow != $i || $month_obj->format('m') != $month )
                            {
                                // Generate invisible block for layout
                                echo '<div class="calendar-day calendar-hide">';
                                echo '</div>';
                                continue;
                            }

                            // Hint current day
                            if ( $now == $month_obj->format('d/m/Y') ) {
                                echo '<div class="calendar-day calendar-today">';
                            } else {
                                echo '<div class="calendar-day">';
                            }

                            $day = $month_obj->format('d');
                            printf('<div>%s</div>', $day);

                            // List events if there are any
                            if ( isset($events[$day]) )
                            {
                                echo '<ul class="calendar-list">';
                                foreach ($events[$day] as $event)
                                {
                                    // Fix use datetime html tag and appropriate attr
                                    echo '<li>['.$event['time'].'] <a href="'.$event['link'].'">'.$event['title'].'</a></li>';
                                }   
                                echo '</ul>';
                            }

                            echo '</div>';
                            
                            $month_obj->add($day_int);
                        }
                        
                        echo '</div>'; // End of week row

                        // Check if we're still in month
                        if ($month_obj->format('m') != $month)
                        {                            
                            echo '</div>'; // End of #calendar
                            break;
                        }
                    }

                    ?>
                </div>
                <header>
                    <?php get_template_part( 'template-parts/events/month_nav' ); ?>
                </header>
            </section>
        </main>
    </div>
</div>

<?php get_footer(); ?>