<?php get_header(); ?>

<div class="row single">
	<div id="page-container">
		<main>
            <section>
            	<header>
                <h1><?= __( 'Events list by month', 'imcpress-theme' ) ?></h1>
                <?php get_template_part('template-parts/events/month_nav') ?>
                </header>
                
                <div class="post-content">
                <?php
                
                while ( have_posts() ) : the_post(); ?>
                    <article class="post">
                        <h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
                        <?php
                        if ( is_event() )
                        {
                            event_card();
                        }
                        ?>
                        <p class="post-meta"><?= posted_by_on(true) ?>
                            <?php
                            // Don't display term we're listing from
                            $filter = (string) single_term_title('', false);

                            list_tax(get_the_id(), 'wire', null, $filter);
                            list_tax(get_the_id(), 'post_tag', null, $filter);
                            list_tax(get_the_id(), 'place_tag', null, $filter);

                            ?>
                        </p>
                        <?php the_excerpt() ?>
                    </article>
                <?php endwhile; ?>

                </div>
            </section>
        </main>
    </div>
</div>

<?php get_footer(); ?>