        </div> <!-- #body -->
        <footer>
            <h3 class="text-center">
                <?= get_option( 'blogname') ?>
            </h3>
            <div class="text-center">
                <p>
                <?= get_option( 'blogdescription') ?>
                </p>

                <?php
                $onion = get_option( 'blogonion');
                if ( !empty( $onion ) )
                {
                    printf( '<p><a href="%1$s">%2$s %1$s</a></p>', $onion, __( 'Connect through TOR:', 'imcpress-theme' ) );
                }

                wp_nav_menu( array(
                    'theme_location' 	=> 'footer-menu',
                    'container'			=> '',
                    // 'items_wrap'		=> '%3$s',
                    'fallback_cb'		=> false,
                ));

                echo IMCPress\FrontendForm\PublishPage::get_page_description( 'footer', '' ); ?>
            </div>
        </footer>
        <?php wp_footer(); ?>
	</body>
</html>