<?php

defined('ABSPATH') or die('Invalid request.');

define('THEME_URI', get_template_directory_uri());
define('ICONS_URI', THEME_URI . '/assets/img/icons/');
define('HOME_URL', get_home_url());

require_once TEMPLATEPATH . '/includes/banner.php';
require_once TEMPLATEPATH . '/includes/inline.php';
require_once TEMPLATEPATH . '/includes/structs.php';
require_once TEMPLATEPATH . '/includes/utils.php';

class IMCTheme {
	public static function setup_theme(): void {
		
		self::theme_support();
		add_action( 'init', array( __CLASS__, 'init') );
		add_filter( 'theme_locale', array( __CLASS__, 'fix_theme_locale'), 10, 2 );

		add_image_size( 'thumb-feature', 9999, 144 );
		add_image_size( 'event-home', 50, 50 );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_assets'), 15 );
		
		add_filter( 'get_the_archive_title_prefix', '__return_empty_string');
		add_filter( 'excerpt_length', array ( __CLASS__, 'excerpt_length') );
		add_filter( 'get_the_excerpt', array( __CLASS__, 'enforce_max_excerpt_length') , 11, 2 );

		add_filter( 'publish_types_descriptions', array( __CLASS__, 'publish_types_descriptions'), 15, 3 );
		add_action( 'pre_publish_form', array( __CLASS__, 'pre_publish_form') );
		add_action( 'post_publish_form', array( __CLASS__, 'post_publish_form') );

		add_action( 'login_head', array( __CLASS__, 'customize_login') );
		add_filter( 'login_headerurl', array( __CLASS__, 'login_headerurl') );
		
		add_filter( 'imcpress_enqueue_ajax_form', '__return_true' );
	}

	public static function theme_support(): void {
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'style', 'script' ) );
		add_theme_support( 'responsive-embeds' );
		add_theme_support( 'menus' );
	}

	public static function init(): void {
		self::load_imcpress_translations();
		
		register_nav_menus(
			array(
			'header-menu' => __( 'Header Menu' ),
			)
		);
		register_nav_menus(
			array(
			'footer-menu' => __( 'Footer Menu' ),
			)
		);
	}

	public static function fix_theme_locale( string $locale, string $domain): string {
		// Because for whatever reason, load_theme_textdomain() wants a 'locale.mo' file
		// where load_plugin_textdomain wants a 'textdomain-locale.mo' file
		// We could name languages files without textdomain but I guess that may add confusion
		return $domain.'-'.$locale;
	}

	public static function load_imcpress_translations(): void {
		load_theme_textdomain( 'imcpress-theme', get_template_directory().'/languages' );
	}

	public static function enqueue_assets(): void {
		wp_enqueue_style( 'imcpress',
			THEME_URI .'/assets/css/imcpress.css'
		);
		
		global $template;
		if ('index.php' == basename($template))
		{
			wp_enqueue_style( 'home',
				THEME_URI.'/assets/css/home.css'
			);
		}

		wp_enqueue_style( 'local',
			THEME_URI .'/assets/css/local.css'
		);
	}

	public static function customize_login(): void {
		wp_enqueue_style( 'imc-login',
			THEME_URI .'/assets/css/login.css'
		);
	}

	public static function login_headerurl( $url ) {
		return home_url();
	}

	// TODO: should move all this excerpt elsewhere
	public static function excerpt_length( int $length ): int {
		if  ( is_admin()) {
			return $length;
		}
		return 50;
	}

	public static function feature_excerpt( $text ) {
		return 230;
	}

	public static function enforce_max_excerpt_length( string $text ) {
		$length = (int) apply_filters( 'excerpt_length', 55 );
		return wp_trim_words( $text, $length);
	}

	public static function publish_types_descriptions( string $content, array $pages, string $publish_page ): string {
		$post['post'] = $pages['post'];
		unset($pages['post']);
		$pages = array_merge( $post, $pages);

		$content = '<div class="publish-container">';
		foreach ($pages as $key => $page )
		{
			$content .= '<div class="publish-description">';
			$content .= sprintf( '<h4 class="center">%s</h4>', $page->label );
			$content .= sprintf(
				'<a href="%s">%s</a>',
				home_url(
					sprintf(
						'%s/%s',
						$publish_page,
						$key) ),
				$page->post_content );
			$content .= '</div>';
		}
		$content .= '</div>';
		
		return $content;
	}

	public static function pre_publish_form(): void {
		?>
		<div class="row single">
			<div id="page-container">
				<main class="post">
					<section>
			<?php
	}

	public static function post_publish_form(): void {
		?>
					</section>
				</main>
			</div>
		</div> 
		<?php
	}

	public static function remove_thumbnail_dimensions( string $html ): string { 
		$html = preg_replace( '/(width|height)=\"\d*\"\s/', '', $html );
		return $html;
	}

	public static function remove_thumbnail_size_attr( array $attr ): array {
		unset( $attr['sizes'] );
		return $attr;
	}
}

add_action( 'after_setup_theme', array( 'IMCTheme', 'setup_theme') );