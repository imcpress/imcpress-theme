<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo wp_title( '&raquo;', false, 'right' ) . get_bloginfo( 'name' ); ?></title>
		<?php wp_head(); ?>
	</head>
    <body>
        <a class="skip-nav" href="#body"><?= __( 'Skip navigation', 'imcpress-theme' ) ?></a>
        <header id="banner">
            <a href="<?= get_home_url() ?>">
				<img alt="" src="<?= get_theme_mod('banner_image',	THEME_URI . '/assets/img/banners/bando10-pirate.png') ?>">
			</a>
        </header>
		<?php get_template_part('template-parts/navbar'); ?>
        <div id="body">