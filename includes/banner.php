<?php

function imcpress_customize_banner( $wp_customize ) {

	$wp_customize->add_setting( 'banner_image' , array(
		'default'   => THEME_URI . '/assets/img/banners/bando10-pirate.png',
		'transport' => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'banner_image',
		array(
			'label' => __( 'Upload banner image', 'imcpress-theme' ),
			'section' => 'title_tagline',
			'settings' => 'banner_image',
			'description' => __( 'Choose the image that will be displayed as the banner on top of the page.', 'imcpress-theme' )
		) ) 	
	 );
}

add_action( 'customize_register', 'imcpress_customize_banner' );