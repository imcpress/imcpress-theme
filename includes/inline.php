<?php

if ( !function_exists('list_tax') )
{
	function list_tax( $post_id, string $taxonomy, int $number = null , string $filter = '', bool $return = false, bool $as_link = true ): ?string
	{
		$tax = get_the_terms($post_id, $taxonomy);

		if (!is_array($tax)) {
			return null;
		}

		if ( $number ) {
			$tax = array_slice( $tax, 0, $number-- );
		}

		$out = '';
		foreach ($tax as $tag)
		{
			if ($filter === $tag->name)
			{
				continue;
			}

			if ( $as_link )
			{
				$out .= sprintf( '<a class="tag %s" href="%s"><img class="tag-icon" src="%s.png" alt=""/>%s</a>',
					$taxonomy,
					get_term_link( $tag ),
					ICONS_URI.$taxonomy,
					$tag->name );
			} else {
				$out .= sprintf( '<span class="tag %s"><img class="tag-icon" src="%s.png" alt=""/>%s</span>',
					$taxonomy,
					ICONS_URI.$taxonomy,
					$tag->name );
			}
		}

		if ($return)
		{
			return $out;
		}

		echo $out;
		return null;
	}
}

if ( !function_exists('current_tab') )
{
	function current_tab( ...$args ): ?string
	{
		foreach (func_get_args() as $arg)
		{
			if ( is_array($arg) )
			{
				foreach ($arg as $k => $v)
				{
					if ( $v === get_query_var($k))
					{
						return ' class="current"';
					}
					// Fix: also check if the_post $k tax term == $v
				}
			}
			elseif ( get_query_var($arg) )
			{
				return ' class="current"';
			}
		}

		return null;
	}
}

if ( !function_exists('posted_by_on') )
{
	function posted_by_on ( bool $short = false ): void
	{
		$time_string = $short ?
			sprintf( '<time class="entry-date published updated" datetime="%s">%s %s</time>', '%1$s', __( 'on', 'imcpress-theme' ), '%2$s' )
			: sprintf('<time class="entry-date published updated" datetime="%s">%s %s</time>', '%1$s', __( 'Published on', 'imcpress-theme' ), '%2$s' );

		if ( !$short && get_the_time( 'U' ) < get_the_modified_time( 'U' ) ) {
			$time_string = sprintf( '%s <time class="entry-date published" datetime="%s">%s</time>, %s <time class="updated" datetime="%s">%s</time>',
				__( 'Published on', 'imcpress-theme' ),
				'%1$s',
				'%2$s',
				__( 'Edited on', 'imcpress-theme' ),
				'%3$s',
				'%4$s',
			);
		}

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);


		if ( ! empty( get_attached_media( '', get_the_ID() ) ) ) {
				printf('<img class="meta icon" src="%sattach.png" alt=""/>',
				ICONS_URI,
			);
		}

		printf(
			'<span class="posted-on"><a href="%1$s" rel="bookmark">%2$s</a></span>',
			esc_url( get_permalink() ),
			$time_string
		);

		if ( is_group( get_the_author_meta( 'ID' ) ) )
		{
			printf('<span class="byline">, %1$s <span class="author vcard"><a class="url fn n" href="%2$s"><img class="meta icon" src="%3$sgroup2.png" alt=""/>%4$s</a></span></span>',
				__( 'by', 'imcpress-theme' ),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				ICONS_URI,
				esc_html( get_the_author() )
			);
		}
		else
		{
			printf('<span class="byline">, %1$s <span class="author vcard"><img class="meta icon" src="%2$suser.png" alt=""/>%3$s</span></span>',
				__( 'by', 'imcpress-theme' ),
				ICONS_URI,
				esc_html( get_the_author() )
			);
		}
	}
}