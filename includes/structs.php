<?php

if ( !function_exists('h2') )
{
	function h2( string $id = '', string $title = '', string $link = '', string $icon = ''): string {
		if ($icon != '')
		{
			$icon = '<img src="'.ICONS_URI.$icon.'" alt="">';
		}
		$openTag = $link ? '<a href="'.$link.'"' : '<span';
		$closeTag = $link ? '</a>' : '</span>';

		ob_start();
		?>
		<input id="<?= $id ?>-collapse" class="toggle" type="checkbox" />
		<h2>
			<?= $openTag ?> class="flex"><?= $icon.$title ?><?= $closeTag ?>
			<label for="<?= $id ?>-collapse"><?= $icon.$title ?></label>
				
			<label for="<?= $id ?>-collapse" class="toggler">
					▾
			</label>
		</h2><?php
		return ob_get_clean();
	}
}

if ( !function_exists('aux_block') )
{
	function aux_block( string $id = '', string $title = '', string $content = '', string $link = '', string $icon = '', array $classes = array() ): void {
		$plus = '';
		if ($link != '')
		{
			$plus = sprintf( '<a href="%s" class="plus">%s →</a>', $link, __( 'More', 'imcpress-theme' ) );
		}

		$h2 = h2($id, $title, $link, $icon);
		$class = ' '.implode(' ', $classes);

		?>
		<aside class="aux-block block <?= $id.$class ?>" id="<?= $id ?>">
			<?= $h2 ?>
			<div class="inner collapsible-content">
				<?= $content ?>
				<?= $plus ?>
			</div>
		</aside><?php
	}
}

if ( !function_exists('block') )
{
	function block( string $id = '', string $title = '', string $content = '', string $link = '', string $icon = ''): void {
		$plus = '';
		if ($link != '')
		{
			$plus = sprintf( '<a href="%s" class="plus">%s →</a>', $link, __( 'More', 'imcpress-theme' ) );
		}

		$h2 = h2($id, $title, $link, $icon);

		?>
		<section class="block <?= $id ?>" id="<?= $id ?>">
			<?= $h2 ?>
			<div class="inner collapsible-content">
				<?= $content ?>
				<?= $plus ?>
			</div>
		</section><?php
	}
}

if ( !function_exists('event_card') )
{
	function event_card(): void {
		$date = get_event_date_time( get_the_id() );
		?>
		<div class="date-card">
			<div class="icon">
				<div class="date">
					<span class="jour"><?= $date->format( 'd' ) ?></span>
					<span class="mois"><?= date_i18n( 'M', $date->format('U')) ?></span>
				</div>
			</div>
			<div class="content">
				<?= '<small>'.__( 'on', 'imcpress-theme' ).' '.date_i18n( __( 'l m/d/Y \a\t g:i a', 'imcpress-theme' ), $date->format( 'U' ) ).'</small><br>' // Fix: display place on several lines
				. nl2br( get_post_meta(get_the_id(), 'imcpress_event_metadata_place', true) ) ?>
			</div>
		</div><?php
	}
}

if ( !function_exists( 'next_group_events') )
{
	function next_group_events( $author, $icon = true ) {
		$query = new WP_Query([
			'author'		=> $author,
			'post_type'		=> 'imcpress_event',
			'post_status'	=> [
				'publish',
				'feature'
			],
			'posts_per_page'	=> '3',
			'meta_key'			=> 'imcpress_event_metadata_date',
			'meta_value'		=> (new DateTime)->format('c'),
			'meta_compare'		=> '>',
			'orderby'			=> 'meta_value',
			'order'				=> 'ASC',
			'no_found_rows'		=> true,
		]);
		
		if($query->have_posts())
		{
			$icon = $icon ? sprintf( '<img class="type-icon" src="%sevent.png" alt="event"/>', ICONS_URI ) : '';
			printf( '<div><h5>%s%s</h5>', $icon, __( 'Next group events:', 'imcpress-theme') );
			echo '<ul>';
			for ( $i = 0 ; $query->have_posts() ; $i++) {
				$query->the_post();
				$date = get_event_date_time( get_the_ID() );

				?>
				<li>
					<span><?= date_i18n( __( 'l m/d:', 'imcpress-theme' ), $date->format( 'U' ) ) ?></span>
					<a href="<?= the_permalink() ?>">
						<?= the_title() ?>
					</a>
				</li>
				<?php 
			}
			echo '</ul></div>';
		}
	}
}