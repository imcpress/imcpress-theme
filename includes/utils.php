<?php

if ( !function_exists('is_group') )
{
	function is_group($id)
	{
		return (bool) get_user_meta($id, 'imcpress_is_group', true);
	}
}

if ( !function_exists('get_query_post_type') )
{
	function get_query_post_type()
	{
		global $wp_query;
		return get_post_type($wp_query->post->ID);
	}
}

if ( !function_exists('is_event') )
{
	function is_event(){
		return 'imcpress_event' == get_query_post_type();
	}
}

if ( !function_exists('is_zine') )
{
	function is_zine(){
		return 'imcpress_zine' == get_query_post_type();
	}
}

if ( !function_exists('get_event_date_time') )
{
	function get_event_date_time( int $id ): DateTime {
		$date = get_post_meta( $id, 'imcpress_event_metadata_date', true );
		return new DateTime($date);
	}
}