<?php get_header(); ?>

<div id="home-layout">
	<?php if(is_404())
	{
		printf( '<h2>%s</h2>', __( 'Page not found' ) );
	}
	?>
    <div class="row" id="row-one">
        <?php get_template_part('template-parts/home/features'); ?>
		<div class="col">
			<?php get_template_part('template-parts/home/events'); ?>
		</div>
    </div>

    <div class="row" id="row-two">
        <div class="col">
            <?php get_template_part('template-parts/home/local'); ?>
        </div>
        <div class="col">
            <?php get_template_part('template-parts/home/simple-block', null, [
                'query' => [
                    'post_type'		=> 'imcpress_tumble',
                    'post_status'	=> [
                        'publish', 'feature'
                    ],
                    'orderby'			=> 'post_date',
                    'order'				=> 'DESC',
                    'posts_per_page'	=> '8',
                    'no_found_rows'		=> true,
                ],
                'slug'  => 'tumbles',
                'title'  => __( 'Tumbles', 'imcpress-theme' ), // May also be "Fil info", pass a context?
                'link'  => get_post_type_archive_link('imcpress_tumble'),
                'icon'  => 'news2.png',

            ]);
            get_template_part('template-parts/home/simple-block', null, [
                'query' => [
                    'post_type'		=> 'imcpress_tumble',
                    'post_status'	=> [
                        'publish', 'feature'
                    ],
                    'orderby'			=> 'post_date',
                    'order'				=> 'DESC',
                    'posts_per_page'	=> '8',
                    'no_found_rows'		=> true,
                ],
                'slug'  => 'podcasts',
                'title'  => __( 'Podcasts', 'imcpress-theme' ),
                'link'  => '',
                'icon'  => 'radio.png',
                'content'   => sprintf( '<p class="no-post">%s</p>', __( 'Coming soon!', 'imcpress-theme' ) )
            ]);
            ?>
        </div>
    </div>

    <div class="row" id="row-three">
        <div class="row">
            <?php get_template_part('template-parts/home/groups'); ?>
            <?php get_template_part('template-parts/home/tags', null, [
                'tax'   => 'post_tag',
                'slug'  => 'tags',
                'title' => __( 'Themes', 'imcpress-theme' ),
                'link'  => 'about',
                'icon'  => 'tag.png'
                ]); ?>
        </div>

        <div class="row">
            <div class="col"> 
                <?php get_template_part('template-parts/home/global'); ?>
            </div>

            <div class="row">
                <div class="col">
                    <?php get_template_part('template-parts/home/zines'); ?>
                    <?php get_template_part('template-parts/home/simple-block', null, [
                        'query' => [
                            'post_type'		=> 'any',
                            'post_status'	=> [
                                'pending',
                            ],
                            'orderby'			=> 'post_date',
                            'order'				=> 'DESC',
                            'posts_per_page'	=> '8',
                            'no_found_rows'		=> true,
                        ],
                        'slug'  => 'awaiting',
                        'title'  => __( 'Awaiting', 'imcpress-theme' ),
                        // Fix: Find appropriate link method?
                        'link'  => '/pending',
                        'icon'  => 'pending.png',
                        'display_title'  => true,

                    ]);
                    get_template_part('template-parts/home/simple-block', null, [
                        'slug'  => 'moderation',
                        'title'  => __( 'Moderation', 'imcpress-theme' ),
                        'link'  => '',
                        'icon'  => 'discussion.png',
                        'display_title'  => true,
                        'content' => sprintf( '<a class="list-el" href="/debate">%s</a>'
                            .'<a class="list-el" href="/refused">%s</a>',
                            __( 'Debating posts', 'imcpress-theme' ), 
                            __( 'Refused posts', 'imcpress-theme' ) ),
                    ]); 

                    ?>
                </div>
                <div class="col">
                    <?php get_template_part('template-parts/home/tags', null, [
                        'tax'   => 'place_tag',
                        'slug'  => 'placetags',
                        'title' => __( 'Places', 'imcpress-theme' ),
                        'link'  => 'in',
                        'icon'  => 'tag.png'
                        ]); ?>
                    
                    <?php 
                    // Fix: temp, will add RSS block template
                    get_template_part('template-parts/home/simple-block', null, [
                        'slug'  => 'elsewhere',
                        'title'  => __( 'Elsewhere', 'imcpress-theme' ), // Fix: change 'elsewhere' wording? (globaly)
                        'link'  => '',
                        'icon'  => 'feed2.png',
                        'display_title'  => true,
                        'content'   => sprintf( '<p class="no-post">%s</p>', __( 'Coming soon!', 'imcpress-theme' ) )

                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>

<?php get_footer();
