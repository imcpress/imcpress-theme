<?php get_header(); ?>
<?php

$per_page = get_option('posts_per_page');
$page = $paged ? $paged : 1;
$offset = ($page - 1) * $per_page;

$query = new WP_User_Query(array(
	'fields'		=> 'all_with_meta',
	'meta_key'		=> 'imcpress_is_group',
	'meta_value'	=> 1,
	'number'		=> $per_page,
	'offset'		=> $offset,
));

$groups = $query->get_results();

$page_args = array(
    'total'        => ceil($query->total_users / $per_page),
    'current'      => $page,
);

?>
<div class="row single">
	<div id="page-container">
		<main>
			<section>
				<header>
				<h1><?= __( 'Groups', 'imcpress-theme' ) ?></h1>

				<?= '<nav class="pagination-container">'.paginate_links($page_args).'</nav>' ?>

				</header>
				<?php 
					foreach( $groups as $id => $group)
					{
						$data = $group->data;
						?>
						<article class="post">
							<h2><a href="<?= get_author_posts_url($id) ?>"><?= $data->display_name ?></a></h2>
							
							<?php 

							$post_query = new WP_Query([
								'author'		=> $id,
								'post_type'		=> array( 'post', 'imcpress_event', 'imcpress_zine'),
								'post_status'	=> [
									'publish',
									'feature'
								],
								'groupby'			=> 'user',
								'orderby'			=> 'post_date',
								'order'				=> 'DESC',
								'posts_per_page'	=> '3',
								'no_found_rows'		=> true,
							]);

							echo '<div class="group">';
							echo get_avatar($id);
							
							echo '<div>';
							printf( '<p>%s</p>', get_user_meta($id, 'description', true) );

							if($post_query->have_posts())
							{
								printf( '<h5>%s</h5>', __( 'Last posts:', 'imcpress-theme' ) );
								echo '<ul>';
								while ( $post_query->have_posts() )
								{
									$post_query->the_post();

									?>
									<li>
										<small><?= get_the_date() ?> :</small>
										<a href="<?= the_permalink() ?>">
											<?= the_title() ?>
										</a>
									</li>
									<?php 
								}
								echo '</ul>';
							}

							next_group_events( $data->ID, false );

							echo '</div></div>';
							?>
						</article>
						<?php
					}
				
				echo '<nav class="pagination-container">'.paginate_links($page_args).'</nav>' ?>
			</section>
		</main>
	</div>
</div>

<?php get_footer(); ?>