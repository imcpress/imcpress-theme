<?php get_header(); ?>

<div class="row single">
	<div id="page-container">
		<main>
            <section>
            	<header>
                    <h1><?= __( 'Tags lists', 'imcpress-theme' ) ?></h1>
                    <p><?= __( 'Only the 2000 most used terms are shown.', 'imcpress-theme' ) ?></p>
                </header>
                <div class="post-content">
                <h2><?= __( 'Themes', 'imcpress-theme') ?></h2>
				<?php
                echo wp_tag_cloud([
                    'number'    => 2000,
                    'echo'		=> false,
                    'taxonomy'	=> 'post_tag',
                    'smallest'	=> 10,
                    'largest'	=> 18,
                    'separator'	=> '',
                    'orderby'	=> 'name',
                    'order'		=> 'ASC'
                ]);?>
                <h2><?= __( 'Places', 'imcpress-theme') ?></h2>
                <?php
                echo wp_tag_cloud([
                    'number'    => 2000,
                    'echo'		=> false,
                    'taxonomy'	=> 'place_tag',
                    'smallest'	=> 10,
                    'largest'	=> 18,
                    'separator'	=> '',
                    'orderby'	=> 'name',
                    'order'		=> 'ASC'
                ]); ?>
                </div>
            </section>
        </main>
    </div>
</div>

<?php get_footer(); ?>