<?php
// Fixes author data not avalaible in this context (whereas it shoudl be?)
// resulting in incomplete posted_by_on() and no the_autor_*()
global $authordata;
$authordata = get_userdata( $post->post_author );

?>
<?php get_header(); ?>

<div class="row double">
	<div id="page-container">
		<main class="post">
            <section>
                <header>
					<h1><?= the_title() ?></h1>
					<hr>
                    <?php posted_by_on();
                    // Don't display "publié le" if status is pending?
                    
                    echo '<br>';

                    $tax = list_tax(get_the_id(), 'wire', null, '', true);
                    if ($tax)
                    {
                        printf( '%s %s<br>', __( 'Category:', 'imcpress-theme' ), $tax );
                    }
                    $tax = list_tax(get_the_id(), 'post_tag', null, '', true);
                    if ($tax)
                    {
                        printf( '%s %s<br>', __( 'Themes:', 'imcpress-theme' ), $tax );
                    }
                    $tax = list_tax(get_the_id(), 'place_tag', null, '', true);
                    if ($tax)
                    {
                        printf( '%s %s<br>', __( 'Places:', 'imcpress-theme' ), $tax );
                    }
                    
                    $status = get_post_status( get_the_ID() );
                    if ( in_array( $status , array( 'pending', 'refused', 'debate' ) ) )
                    { 
                        switch ($status)
                        {
                            case 'pending':
                                $s_status = __( 'awaiting', 'imcpress-theme' );
                                break;
                            case 'refused':
                                $s_status = __( 'refused', 'imcpress-theme' );
                                break;
                            case 'debate':
                                $s_status = __( 'in debate', 'imcpress-theme' );
                                break;
                        }
                        printf(
                            '<div class="alert">%s %s.</div>',
                            __( 'This post is', 'imcpress-theme' ),
                            $s_status
                        );
                    }

                    if (is_event())
                    {
                        event_card();
                    }


                    if (is_group($post->post_author))
                    {
                        echo '<div class="group-container">';
                        // Fix: get actual default avatar?
                        if ('user.png' != basename(get_avatar_url($post->post_author)) )
                        {
                            echo get_avatar($post->post_author, 96, '', '', array( 'class' => 'avatar') );
                        }
                        ?>
                            <div>
                                <div class="group-name"><?= get_the_author() ?></div>
                                <div class="group-description"><?= get_the_author_meta('description') ?></div>
                            </div>
                        </div><?php
                    }

					?>
				<hr>
                </header>
                <div class="post-content">
                    <?php
                    if ( is_zine() )
					{
						$thumbnail = get_the_post_thumbnail( get_the_ID(), 'medium' );
						$pdf_url = array_values(get_attached_media( 'application/pdf', get_the_ID() ))[0]->guid;
                        printf( '<a href="%s">%s</a>', $pdf_url, $thumbnail );
					}
					the_content();
					?>

                </div>
                <?php
                if ( !is_zine() )
				{
					if ( has_post_thumbnail() ) {
						echo '<hr>';
						echo '<header>';
						printf( '<h4>%s</h4>', __( 'Gallery' ) ); // i10n: probably exists in wp textdomain?

						echo gallery_shortcode( array( 'link' => 'file', 'columns' => '4' ) );
						echo '</header>';
					}
				}
                $media = get_attached_media( '', get_the_ID() );
                if ( !empty($media) ) {
                    echo '<hr style="clear: both;">';
					echo '<header>';
                    printf( '<h4>%s</h4>', __( 'Attachments', 'imcpress-theme' ) ); // i10n: probably exists in wp textdomain?
                    echo '<ul>';
                    foreach ( $media as $medium) {
                        printf( '<li><a href="%s">%s</a></li>', $medium->guid, $medium->post_title );
                    }
                    echo '</ul>';
					echo '</header>';
                }
                ?>
            </section>
        </main>
        <section>
            <div class="comments">
                <?php comments_template() ?>
            </div>
        </section>
    </div>
    <div>
        <?php get_template_part('template-parts/home/events'); ?>
		<?php
			get_template_part('template-parts/home/simple-block', null, [
                'query' => [
                    'post_type'		=> 'imcpress_tumble',
                    'post_status'	=> [
                        'publish', 'feature'
                    ],
                    'orderby'			=> 'post_date',
                    'order'				=> 'DESC',
                    'posts_per_page'	=> '8',
                    'no_found_rows'		=> true,
                ],
                'slug'  => 'tumbles',
                'title'  => __( 'Tumbles', 'imcpress-theme' ), // May also be "Fil info", pass a context?
                'link'  => get_post_type_archive_link('imcpress_tumble'),
                'icon'  => 'news2.png',
				'class'	=> array( 'majenta' ),
            ]);
		?>
        <?php get_template_part('template-parts/home/related'); ?>
    </div>
</div>

<?php get_footer();
