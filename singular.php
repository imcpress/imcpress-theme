<?php get_header(); ?>

<div class="row single">
	<div id="page-container">
		<main>
            <section>
                <h1><?= the_title() ?></h1>
                <div class="post-content">
                <?= the_content() ?>
                </div>
            </section>
        </main>
    </div>
</div>

<?php get_footer();