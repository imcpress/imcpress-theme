<nav>
	<?php $current = get_query_var( 'event_view' ); ?>
	<?php if ( !empty($current) ): ?>
		<a class="tag" href="/events"><?= __( 'Upcoming events', 'imcpress-theme' )?></a>
	<?php endif; if ( 'calendar' != $current ): ?>
		<a class="tag" href="/events/calendar"><img class="type-icon" src="<?= ICONS_URI ?>calendar2.png" alt=""><?= __( 'Calendar view', 'imcpress-theme' ) ?></a>
	<?php endif; if ( 'archive' != $current ): ?>
		<a class="tag" href="/events/archive"><?=__( 'All times events', 'imcpress-theme' ) ?></a>
	<?php endif; ?>
</nav>