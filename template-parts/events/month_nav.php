<?php 
global $wp, $year, $month;
$qv = &$wp->query_vars;
$year = $qv['year'] ?? null;
$month = $qv['monthnum'] ?? null;

// If no date specified in URI
$date = ( !$year || !$month ) ?
	new \DateTime() :
	new \DateTime( sprintf( '%04d-%02d-01', $year, $month ) );

// Set global var for later use in template
$year = $date->format('Y');
$month = $date->format('m');


// Fix: set actual links
// link calendar/list
echo '<nav>';
	
	$date->modify('-1 month');
	$y = $date->format('Y');
	$m = $date->format('m');
	echo "<a class=\"tag\" href=\"/events/calendar/$y/$m\">« ". $date->format('F')."</a>";
	
	$date->modify('+1 month');
	echo ' '. $date->format('M Y') . ' ';
	
	$date->modify('+1 month');
	$y = $date->format('Y');
	$m = $date->format('m');
	echo "<a class=\"tag\" href=\"/events/calendar/$y/$m\">". $date->format('F')." »</a>";

	echo '</nav>';