<?php
// Fix: date handlers?
$now = new Datetime();

$query = array(
	'post_type' => 'imcpress_event',
	'post_status' => array ('publish', 'feature', 'pending' ),
	'meta_key'	=> 'imcpress_event_metadata_date',
	'meta_value' => $now->format('c'),
	'meta_compare' => '>',
	'orderby' => 'meta_value',
	'order'	=> 'ASC',
	'posts_per_page'	=> 12,
	'no_found_rows' => true,
	);

if ( isset($args['query']) ) {
	$query = wp_parse_args( $args['query'], $query);
}

$query = new WP_Query( $query );

$now = $now->format('d/m/Y');
$tomorrow = (new DateTime('tomorrow'))->format('d/m/Y');

$content = '';
ob_start();
while ($query->have_posts()) {
	$query->the_post();

	$date = get_event_date_time( get_the_id() );

	$class = '';
	$date_fmt = $date->format('d/m/Y');
	if ($date_fmt == $now) {
		$class = ' today';
	} else if ($date_fmt == $tomorrow) {
		$class = ' tomorrow';
	}

	?>
	<a class="list-el event<?= $class ?>" href="<?= the_permalink() ?>">
		<div class="icon">
			<div class="date">
				<span class="jour"><?= $date->format( 'd' ) ?></span>
				<span class="mois"><?= date_i18n( 'M', $date->format('U')) ?></span>
			</div>
		</div>
			
		<div class="content">
			<?= the_title() ?><br>
			<?php
			$list = list_tax( $query->get_theID(), 'place_tag', 1, '', true, false );
			if ( !empty( $list ) ) {
				printf( '%s<br>', $list);
			}
			?>
			<small><?= __( 'on', 'imcpress-theme' ) ?> <?= date_i18n( __( 'l m/d \a\t g:i a', 'imcpress-theme' ), $date->format( 'U' ) ) ?></small>
		</div>
	</a><?php
}
wp_reset_postdata();
$content = ob_get_clean();

if (empty($content)){
	$content = sprintf( '<div class="no-post">%s</div>', __( 'No events at the moment', 'imcpress-theme' ) );
}

$link = empty( $query->tax_query->queries ) ? 
	trailingslashit( get_post_type_archive_link( 'imcpress_event' ) ) . 'calendar/' :
	sprintf(
		'%s%s/%s',
		trailingslashit( get_post_type_archive_link( 'imcpress_event' ) ),
		\IMCPress\Rewrite::get_tax_rewrite_slug( $query->tax_query->queries[0]['taxonomy'] ),
		$query->tax_query->queries[0]['terms'][0] );
aux_block('events', __( 'Events', 'imcpress-theme' ), $content, $link, 'calendar3.png');