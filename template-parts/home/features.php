<?php
$query = new WP_Query([
	'post_type' => ['post', 'imcpress_event'],
	'post_status' => 'feature',
	'meta_key'	=> 'imcpress_feature_date',
	'orderby' => 'meta_value',
	'order'	=> 'DESC',
	'posts_per_page'	=> 5,
	'no_found_rows' => true,
	]);


add_filter( 'post_thumbnail_html', array( 'IMCTheme', 'remove_thumbnail_dimensions' ), 10 ); 
add_filter( 'image_send_to_editor', array( 'IMCTheme', 'remove_thumbnail_dimensions' ), 10 );

echo '<div class="col" id="features">';
if ($query->have_posts()) {

	add_filter( 'excerpt_length', array( '\IMCTheme', 'feature_excerpt' ) );
	for ($i = 1 ; $query->have_posts() ; $i++)
	{
		$query->the_post();

		if ($i == 1 || $i == 3) {
			echo '<div class="row">';
		}
		
		$id_attr = '';
		if ($i == 1) {
			$id_attr = ' id="main-feature"';
		}
		
		$class = '';
		if ($i > 3) {
			$class = ' hide-phone';
		}

		?><div class="feature list-el<?= $class ?>"<?= $id_attr ?>>
			<a href="<?php the_permalink()?>">
				<div class="img-container">
					<?php the_post_thumbnail('large', ['class' => 'image-feature']); ?>
				</div>
			</a>

			<div class="feature-content">
				<a href="<?php the_permalink()?>">
					<h3><?php the_title() ?></h3>
				</a>

				<small>
				<?php
				posted_by_on(true);
				echo '</small>';

				if ($i > 1) {
					echo '<br />';
				}
				list_tax(get_the_id(), 'post_tag', 1);
				list_tax(get_the_id(), 'place_tag', 1);

				if (get_post_type() == 'imcpress_event')
				{
					event_card();
				}

				echo '<div class="feature-content-wrapper">';
				the_excerpt();
				echo '</div>';
				?>
			</div>
				
			<a href="<?php the_permalink() ?>">
				<div class="plus"><div><?= __( 'Read more', 'imcpress-theme') ?> →</div></div>
			</a>
		</div><?php

		if ($i == 2 || $i == 5  || $i === $query->post_count ) {
			echo '</div>';
		}
	}
	remove_filter( 'excerpt_length', array( '\IMCTheme', 'feature_excerpt' ) );
}
else {
	printf( '<div class="no-post">%s</div>', __( 'No posts at the moment', 'imcpress-theme' ) );
}
echo '</div>';
wp_reset_postdata();

remove_filter( 'post_thumbnail_html', array( 'IMCTheme', 'remove_thumbnail_dimensions' ), 10 ); 
remove_filter( 'image_send_to_editor', array( 'IMCTheme', 'remove_thumbnail_dimensions' ), 10 );