<?php

$query = new WP_Query([
	'post_type'		=> 'post',
	'post_status'	=> [
			'publish',
			'feature'
	],
	'tax_query' => [[
		'taxonomy' => 'wire',
		'field'	=> 'slug',
		'terms'	=> 'global']],
	'orderby'			=> 'post_date',
	'order'				=> 'DESC',
	'posts_per_page'	=> '12',
	'no_found_rows'		=> true,
]);

// Fix: cards with no img and not wide enough text don't get the correct size
ob_start();
echo '<div class="content">';
while ($query->have_posts()) {
	$query->the_post();

	?>
	<div class="global list-el">
		<a href="<?= the_permalink() ?>">
		<?php if (has_post_thumbnail()) {
				the_post_thumbnail('thumbnail', ['class' => 'image-global']);
			}?>
			<h4><?= the_title() ?></h4>
		</a>
		<div class="grid">
			<div>
				<small> <?php posted_by_on(true) ?></small>
			</div>
		</div>
	</div>
	<?php 
}
if ($query->post_count == 0) {
	printf( '<div class="no-post">%s</div>', __( 'No posts at the moment', 'imcpress-theme' ) );
}
echo '</div>';
wp_reset_postdata();
$content = ob_get_clean();

// We may also use wire term description
block('globals', __( 'Global news', 'imcpress-theme' ), $content, get_term_link( 'global', 'wire' ), 'planete.png');
