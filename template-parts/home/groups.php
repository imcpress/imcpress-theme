<?php

global $wpdb;
$meta_key = 'imcpress_last_post';
$last_authors = $wpdb->get_col(
    $wpdb->prepare(
		// Fix: is cast syntax universal? this works with mariadb/mysql
        "
            SELECT user_id
            FROM $wpdb->usermeta
            WHERE meta_key = %s
			AND user_id IN
				(SELECT user_id
				FROM wp_usermeta
				WHERE meta_key = 'imcpress_is_group'
				AND meta_value = '1')
			ORDER BY CAST(meta_value AS datetime) DESC
			LIMIT 6
        ",
        $meta_key
    )
);


ob_start();
if (!empty($last_authors))
{
	foreach($last_authors as $author)
	{
		$query = new WP_Query([
			'author'		=> $author,
			'post_type'		=> array( 'post', 'imcpress_event', 'imcpress_zine'),
			'post_status'	=> [
				'publish',
				'feature'
			],
			'groupby'			=> 'user',
			'orderby'			=> 'post_date',
			'order'				=> 'DESC',
			'posts_per_page'	=> '3',
			'no_found_rows'		=> true,
		]);
		
		if($query->have_posts())
		{
			for ( $i = 0 ; $query->have_posts() ; $i++) {
				$query->the_post();

				// I put this inside the post loop to avoid fetching authors infos manualy
				if (!$i)
				{ ?>
					<div>
						<h6>
							<a href="<?= get_author_posts_url($author) ?>"><?= get_the_author() ?></a>
						</h6>
						<div class="group">
							<a href="<?= get_author_posts_url($author) ?>">
								<?= get_avatar($author) ?>
							</a>
							<ul>
					<?php
				}

				?>
				<li>
					<small><?= get_the_date( __( 'm/d:', 'imcpress-theme' ) ) ?></small>
					<a href="<?= the_permalink() ?>">
						<?= the_title() ?>
					</a>
				</li>
				<?php 
			}
			echo '</ul></div></div>';
		}
	}
}

$content = ob_get_clean();

if (empty($content))
{
	$content = sprintf( '<div class="no-post">%s</div>', __( 'No posts at the moment', 'imcpress-theme' ) );
}
$content = sprintf( '<div class="content">%s</div>', $content );

aux_block('groups', __( 'Groups', 'imcpress-theme' ), $content, 'groups', 'group2.png');