<?php

$query = new WP_Query([
	'post_type'		=> 'post',
	'post_status'	=> [
			'publish',
			'feature'
	],
	'tax_query' => [[
		'taxonomy' => 'wire',
		'field'	=> 'slug',
		'terms'	=> 'local']],
	'orderby'			=> 'post_date',
	'order'				=> 'DESC',
	'posts_per_page'	=> '12',
	'no_found_rows'		=> true,
]);

// Fix: cards with no img and not wide enough text don't get the correct size
ob_start();
echo '<div class="content">';
while ($query->have_posts()) {
	$query->the_post();

	?>
	<div class="local list-el">
		<a href="<?= the_permalink() ?>">
			<h3><?= the_title() ?></h3>
		</a>
		<small> <?php posted_by_on(true) ?></small>
		<?php list_tax(get_the_id(), 'post_tag', 1 ); list_tax(get_the_id(), 'place_tag', 1 )?>
		<div>
			<?php if (has_post_thumbnail()) {
				printf( '<a href="%s">%s</a>', get_the_permalink(), get_the_post_thumbnail( null, 'thumbnail', ['class' => 'image-local'] ) );
			}?>
			<?= the_excerpt() ?>
			<a href="<?= the_permalink() ?>" class="plus"><?= __( 'Read more', 'imcpress-theme') ?> →</a>
		</div>
	</div>
	<?php 
}
if ($query->post_count == 0) {
	printf( '<div class="no-post">%s</div>', __( 'No posts at the moment', 'imcpress-theme' ) );
}
echo '</div>';
wp_reset_postdata();
$content = ob_get_clean();

// We may also use wire term description here
block('locals', __( 'Local news', 'imcpress-theme' ), $content, get_term_link('local', 'wire'), 'house2.png');
