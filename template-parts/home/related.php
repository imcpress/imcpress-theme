<?php
$taxonomies = array( 'post_tag', 'place_tag' );
foreach ($taxonomies as $taxo)
{
	$terms = get_the_terms( get_the_ID(), $taxo );

	if ( !empty( $terms ) )
	{
		foreach ($terms as $term)
		{
			$taxes[$taxo][] = $term->slug;
		}
	}
	else
	{
		$taxes[$taxo] = array();
	}
}

$query = array(
	'post_type'		=> 'post',
	'post_status'	=> [
			'publish',
			'feature'
	],
	'post__not_in'		=> array( get_the_ID() ),
	'orderby'			=> 'post_date',
	'order'				=> 'DESC',
	'posts_per_page'	=> '12',
	'no_found_rows'		=> true,
	'tax_query'			=> array(
		'relation'	=> 'OR',
		array(
			'taxonomy'	=> 'post_tag',
			'field'		=> 'slug',
			'terms'		=> $taxes['post_tag'], // Put terms
		),
		array(
			'taxonomy'	=> 'place_tag',
			'field'		=> 'slug',
			'terms'		=> $taxes['place_tag'], // Put terms
		),
	),
);

get_template_part('template-parts/home/simple-block', null, array(
	'query'			=> $query,
	'slug' 			=> 'related',
	'title'			=> __( 'Related', 'imcpress-theme' ),
	'link' 			=> '',
	'icon' 			=> 'feed2.png',
	'display_title' => true,
) );