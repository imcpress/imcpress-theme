<?php

if ( !isset($args['content']) )
{
	$query = new WP_Query($args['query']);

	// Fix: margins
	ob_start();
	while ($query->have_posts()) {
		$query->the_post();
		
		?>
		<a href="<?= the_permalink() ?>" class="list-el">
			<div class="icon">
				<?php
				if (has_post_thumbnail()) {
					the_post_thumbnail('thumbnail');
				}?>
			</div>
			<?php // Fix: put those inline css in .css if we like it ?>
			<div class="content" style="width: 100%;">
				<?php 
				if (isset($args['display_title']) && $args['display_title'])
				{
					echo '<p>';
					the_title();
					echo '</p>';
				}
				else
				{
					the_excerpt();
				}?>
				<span class="date" style="text-align: right; display: inline-block; width: 100%;"><?= date_i18n( __( 'l m/d/Y \a\t g:i a', 'imcpress-theme' ), get_the_date('U') ) ?></span>
			</div>
		</a>
		<?php 
	}
	if ($query->post_count == 0) {
		printf( '<div class="no-post">%s</div>', __( 'No posts at the moment', 'imcpress-theme' ) );
	}
	$content = ob_get_clean();
}
else
{
	$content = $args['content'];
}

aux_block($args['slug'], $args['title'], $content, $args['link'], $args['icon'], $args['class'] ?? array());