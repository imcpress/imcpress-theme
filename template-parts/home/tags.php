<?php

ob_start();
echo '<div class="content">';
$cloud = wp_tag_cloud([
	'echo'		=> false,
	'number'	=> 21,
	'taxonomy'	=> $args['tax'],
	'smallest'	=> 9,
	'largest'	=> 18,
	'separator'	=> '',
	'orderby'	=> 'count',	// Warning: Throw a depreciation warning in PHP8
	'order'		=> 'RAND'
]);
echo $cloud;
echo '</div>';
$content = ob_get_clean();

aux_block($args['slug'], $args['title'], $content, $args['link'], $args['icon']);
