<?php
$nb_zines = 4;

$query = new WP_Query(array(
	'post_type'		=> 'imcpress_zine',
	'post_status'	=> array (
		'publish',
	),
	'orderby'			=> 'post_date',
	'order'				=> 'DESC',
	'posts_per_page'	=> $nb_zines,
	'no_found_rows'		=> true,
	));


add_filter( 'post_thumbnail_html', array( 'IMCTheme', 'remove_thumbnail_dimensions' ), 10 ); 
add_filter( 'image_send_to_editor', array( 'IMCTheme', 'remove_thumbnail_dimensions' ), 10 );

ob_start();
echo '<div class="content">';

if ($query->post_count != 0) {
?>
<section class="carousel" aria-label="Gallery">
	<div class="slides-container">
		<?php 

		$i = 1;
		while ($query->have_posts()) {
			$query->the_post();

			if ($i == 1 ) {
				$h = $nb_zines;
				$j = $i + 1;
			} else if ( $i == $nb_zines ) {
				$h = $i - 1;
				$j = 1;
			} else {
				$h = $i - 1;
				$j = $i + 1;
			}

			?>
			<div id="zine-<?= $i ?>" class="carousel-slide">

				<a class="carousel-nav prev" href="#zine-<?= $h ?>"><span>◀</span></a>
				
				<div class="slide-content">
					<a href="<?= the_permalink() ?>">
						<h3><?= the_title() ?></h3>
					</a>
					<div class="meta">
						<small> <?php posted_by_on(true) ?></small>
						<?php list_tax(get_the_id(), 'post_tag') ?>
					</div>

						<?php if (has_post_thumbnail()) {
							the_post_thumbnail('medium', ['class' => 'carousel-image']);
						}?>
						<div>
							<?= the_excerpt() ?>
							<a href="<?= the_permalink() ?>" class="plus"><?= __( 'Read the zine', 'imcpress-theme' ) ?> →</a>
						</div>
				</div>

				<a class="carousel-nav next" href="#zine-<?= $j ?>"><span>▶</span></a>

			</div>

			<?php 
			$i++;
		}
		?>
	</div>
</section>
<?php

} else
{
	printf( '<div class="no-post">%s</div>', __( 'No posts at the moment', 'imcpress-theme' ) );
}
echo '</div>';
wp_reset_postdata();
$content = ob_get_clean();

remove_filter( 'post_thumbnail_html', array( 'IMCTheme', 'remove_thumbnail_dimensions' ), 10 ); 
remove_filter( 'image_send_to_editor', array( 'IMCTheme', 'remove_thumbnail_dimensions' ), 10 );

// We may also get type label here
aux_block('zines', __( 'Zines', 'imcpress-theme' ), $content, get_post_type_archive_link('imcpress_zine'), 'zine.png');