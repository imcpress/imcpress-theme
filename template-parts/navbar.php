<nav id="main-navbar">
	<div class="navbar-header">
		<a href="/">
			<img alt="<?= __( 'Home', 'imcpress-theme') ?>" src="<?= ICONS_URI ?>free-indymedia.png">
		</a>
		<a href="<?= HOME_URL ?>/events/calendar/">
			<img alt="<?= trailingslashit( get_post_type_archive_link( 'imcpress_event' ) ) ?>calendar/" src="<?= ICONS_URI ?>calendar.png">
		</a>
		<?php
			$user = wp_get_current_user();
			$log_link = ( 0 != $user->ID ) ? get_dashboard_url( $user->ID ) : wp_login_url();
			$log_txt = ( 0!= $user->ID ) ? __( 'My posts', 'imcpress-theme') : __( 'Log in', 'imcpress-theme' );
		?>
		<a href="<?= $log_link ?>">
			<img alt="<?= __( 'Log in', 'imcpress-theme') ?>" src="<?= ICONS_URI ?>keyhole.png">
		</a>

		<?php
			$publish = get_option( 'publish_page_slug' );
			$publish = $publish ? $publish : 'publish';
			$publish = '/' . $publish;
		?>
		<a href="<?= HOME_URL . $publish ?>" class="m-left-auto publish-btn">
			<img alt="<?= __( 'Publish', 'imcpress-theme') ?>" src="<?= ICONS_URI ?>plume.png">
		</a>
		<label class="hamburger toggler" for="nav-collapse">
			<img alt="<?= __( 'Display menu', 'imcpress-theme') ?>" src="<?= ICONS_URI ?>hamburger.png">
		</label>
	</div>
		
	<input id="nav-collapse" class="toggle" type="checkbox" />
	<div class="collapsible-content" id="menu-collapse">
		<ul>
			<li id="indy-logo" class="hide-phone no-padding">
				<a href="<?= HOME_URL ?>/"><img src="<?= ICONS_URI ?>indy-logo-negatif.png" title="<?= __( 'Home', 'imcpress-theme') ?>" alt="<?= __( 'Home', 'imcpress-theme') ?>"></a>
			</li>

			<li id="events-tab"<?= current_tab( array( 'post_type' => 'imcpress_event') ) ?>>
				<a href="<?= trailingslashit( get_post_type_archive_link( 'imcpress_event' ) ) ?>calendar/"><?= __( 'Events', 'imcpress-theme' ) ?></a>
			</li>
			
			<li id="local-tab"<?= current_tab( array('wire' => 'local') ) ?>>
				<a href="<?= get_term_link('local', 'wire') ?>"><?= __( 'Local', 'imcpress-theme' ) ?></a>
			</li>
			
			<li id="tumble-tab"<?= current_tab( array('post_type' => 'imcpress_tumble') ) ?>>
				<a href="<?= get_post_type_archive_link('imcpress_tumble') ?>"><?= __( 'Tumbles', 'imcpress-theme' ) ?></a>
			</li>
			
			<li id="groups-tab"<?= current_tab( 'author_name', array( 'list' => 'groups' ) ) ?>>
				<a href="<?= HOME_URL ?>/groups/"><?= __( 'Groups', 'imcpress-theme' ) ?></a>
			</li>
			
			<li id="zines-tab"<?= current_tab( array( 'post_type' => 'imcpress_zine') ) ?>>
				<a href="<?= get_post_type_archive_link('imcpress_zine') ?>"><?= __( 'Zines', 'imcpress-theme' ) ?></a>
			</li>
			
			<li id="global-tab"<?= current_tab( array('wire' => 'global') ) ?>>
				<a href="<?= get_term_link('global', 'wire') ?>"><?=__( 'Global', 'imcpress-theme' ) ?></a>
			</li>

			<li id="elsewhere-tab" class="hidden">
				<a href="#elsewhere"><?= __( 'Elsewhere', 'imcpress-theme' ) ?></a>
			</li>

			<li id="tags-tab"<?= current_tab('tag', 'place_tag', array( 'list' => 'about' ), array( 'list' => 'in' ) ) ?>>
				<a href="<?= HOME_URL ?>/about/"><?= __( 'Tags', 'imcpress-theme' ) ?></a>
			</li>

			<?php wp_nav_menu( array(
				'theme_location' 	=> 'header-menu',
				'container'			=> '',
				'items_wrap'		=> '%3$s',
				'fallback_cb'		=> false,
			 )); ?>

			<li id="publish-tab" class="m-left-auto">
				<?php // Fix: replace by publish-slug option ?>
				<a class="btn hide-phone" href="<?= HOME_URL . $publish ?>"><?= __( 'Publish', 'imcpress-theme' ) ?></a>
			</li>
			<li id="login-tab">
				<a class="btn hide-phone" href="<?= $log_link ?>"><?= $log_txt ?></a>
			</li>
		</ul>
	</div>
</nav>